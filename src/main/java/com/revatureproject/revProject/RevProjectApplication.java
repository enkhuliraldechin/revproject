package com.revatureproject.revProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RevProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RevProjectApplication.class, args);
	}

}
